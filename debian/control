Source: libkdegames
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Daniel Schepler <schepler@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Modestas Vainius <modax@debian.org>,
           George Kiagiadakis <gkiagiad@csd.uoc.gr>,
           Eshat Cakar <info@eshat.de>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: 7zip,
               cmake,
               debhelper-compat (= 13),
               kf6-extra-cmake-modules,
               kf6-karchive-dev,
               kf6-kbookmarks-dev,
               kf6-kcodecs-dev,
               kf6-kcolorscheme-dev,
               kf6-kcompletion-dev,
               kf6-kconfig-dev,
               kf6-kconfigwidgets-dev,
               kf6-kcoreaddons-dev,
               kf6-kcrash-dev,
               kf6-kdbusaddons-dev,
               kf6-kdeclarative-dev,
               kf6-kdnssd-dev,
               kf6-kglobalaccel-dev,
               kf6-kguiaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kio-dev,
               kf6-kitemviews-dev,
               kf6-kjobwidgets-dev,
               kf6-knewstuff-dev,
               kf6-kservice-dev,
               kf6-ktextwidgets-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kxmlgui-dev,
               libopenal-dev,
               libsndfile1-dev,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-svg-dev,
               xauth,
               xvfb
Standards-Version: 4.6.2
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/libkdegames
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/libkdegames.git

Package: kdegames-card-data-kf6
Architecture: all
Section: games
Depends: ${misc:Depends}
Breaks: kdegames-card-data-kf5
Replaces: kdegames-card-data-kf5
Description: card decks for KDE games
 This package contains a collection of playing card themes for KDE card games.
 .
 This package is part of the KDE games module.

Package: libkdegames6-dev
Architecture: any
Section: libdevel
Depends: kf6-kcompletion-dev,
         kf6-kconfigwidgets-dev,
         kf6-kdeclarative-dev,
         libkf6kdegames6-6 (= ${binary:Version}),
         libkf6kdegames6private6 (= ${binary:Version}),
         qt6-base-dev,
         ${misc:Depends}
Breaks: libkf5kdegames-dev (<< 4:24.02)
Replaces: libkf5kdegames-dev (<< 4:24.02)
Description: development files for the KDE games library
 This package contains development files for building software that uses
 libraries from the KDE games module.
 .
 This package is part of the KDE games module.

Package: libkf6kdegames6-6
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5kdegames7 (<< 4:24.02)
Replaces: libkf5kdegames7 (<< 4:24.02)
Description: shared library for KDE games
 This package contains a shared library used by KDE games.
 .
 This package is part of the KDE games module.

Package: libkf6kdegames6private6
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libkf5kdegamesprivate7 (<< 4:24.02)
Replaces: libkf5kdegamesprivate7 (<< 4:24.02)
Description: private part of shared library for KDE games
 This package contains the private parts of the shared library used by KDE
 games.
 .
 This package is part of the KDE games module.

Package: qml6-module-org-kde-games-core
Architecture: any
Depends: qml6-module-qtquick, ${misc:Depends}, ${shlibs:Depends}
Breaks: qml-module-org-kde-games-core (<< 4:24.02)
Replaces: qml-module-org-kde-games-core (<< 4:24.02)
Description: kde-games-core QtDeclarative QML support
 Contains a plugin for QtDeclarative that provides
 support for using KDE-games-core to components written
 in QML.
 .
 This package is part of the KDE games module.

Package: libkf5kdegames-dev
Architecture: all
Priority: optional
Section: oldlibs
Depends: ${misc:Depends}
Description: dummy transitional
 Remove this.

Package: libkf5kdegames7
Architecture: all
Priority: optional
Section: oldlibs
Depends: ${misc:Depends}
Description: dummy transitional
 This can be removed.

Package: libkf5kdegamesprivate7
Architecture: all
Priority: optional
Section: oldlibs
Depends: ${misc:Depends}
Description: dummy transitional
 This can be removed.

Package: qml-module-org-kde-games-core
Architecture: all
Priority: optional
Section: oldlibs
Depends: ${misc:Depends}
Description: dummy transitional
 This can be removed.
